import {Component, OnInit} from '@angular/core';
import {Customer} from '../customer';
import {CustomerService} from '../service/customer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  customer: Customer;

  constructor(private customerService: CustomerService,
              private router: Router) {
  }

  ngOnInit() {
    this.customer = new Customer();
  }

  handleSubmit() {
    this.customerService.addCustomer(this.customer).subscribe(
      data => this.router.navigate(['customers'])
    );
  }

}
