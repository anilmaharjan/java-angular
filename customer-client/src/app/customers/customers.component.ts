import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../service/customer.service';
import {Customer} from '../customer';
import {Router} from '@angular/router';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  customers: Customer[];
 customerCount = 0;
  constructor(private customerService: CustomerService,
              private router: Router) {
  }

  ngOnInit() {
    this.refreshCustomers();
  }

  refreshCustomers() {
    this.customerService.getAllCustomers().subscribe(
      data => {
        this.customers = data;
        console.log(this.customers);
      }
    );
  }

  handleDelete(id) {
    this.customerService.deleteCustomer(id).subscribe(
      response => this.refreshCustomers()
    );
  }

  handleUpdate(id) {
   this.customerService.updateCustomer(id).subscribe(res => console.log(res));
  }

  handleAdd() {
    this.customerService.addCustomer(this.customer);
  }
}
