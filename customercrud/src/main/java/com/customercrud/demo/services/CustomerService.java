package com.customercrud.demo.services;

import com.customercrud.demo.entity.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer findById(Integer id);
    void addCustomer(Customer customer);
    void updateCustomer(Integer id, Customer customer);
    void deleteCustomer(Integer id);
}
