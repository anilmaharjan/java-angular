import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CustomersComponent} from './customers/customers.component';
import {UpdateComponent} from './update/update.component';
import {AddComponent} from './add/add.component';

const routes: Routes = [
  {path: '', component: CustomersComponent},
  {path: 'customers', component: CustomersComponent},
  {path: 'update/:id', component: UpdateComponent},
  {path: 'add', component: AddComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
