import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Customer} from '../customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private customersUrl = 'http://localhost:8080/api/customers';

  constructor(private http: HttpClient) {
  }

  getAllCustomers() {
    return this.http.get<Customer[]>(this.customersUrl);
  }

  deleteCustomer(id) {
    const url = `${this.customersUrl}/${id}`;
    return this.http.delete(url);
  }

  updateCustomer(id, customer) {
    const url = `${this.customersUrl}/${id}`;
    return this.http.put<Customer>(url, customer);
  }

  getCustomerById(id) {
    const url = `${this.customersUrl}/${id}`;
    return this.http.get<Customer>(url);
  }

  addCustomer(customer) {
    const url = `${this.customersUrl}`;
    return this.http.post(url, customer);
  }
}
